List cetakHurufVokal(String name) {
  var vokal = ["a", "i", "u", "e", "o"];
  var strSplit = name.split('');
  var count = 0;
  var container = [];
  for (int i = 0; i < name.length; i++) {
    if (vokal.contains(strSplit[i]) && !container.contains(strSplit[i])) {
      count++;
      container.add(strSplit[i]);
    }
  }
  var result = container.join(', ');
  return [count, result];
}

void main() {
  var result = cetakHurufVokal("kevin");
  print("${result[0]} yaitu ${result[1]}");
}
