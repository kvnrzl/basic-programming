cetakGanjilGenap(int a, int b) {
  for (int i = a; i <= b; i++) {
    i % 2 == 0
        ? print("Angka $i adalah genap")
        : print("Angka $i adalah ganjil");
  }
}

void main() {
  cetakGanjilGenap(1, 4);
}
