kalkulatorSederhana(String a) {
  var strSplit = a.contains(" ") ? a.split(" ") : a.split('');
  return strSplit.contains("+")
      ? int.parse(strSplit[0]) + int.parse(strSplit[2])
      : strSplit.contains("-")
          ? int.parse(strSplit[0]) - int.parse(strSplit[2])
          : strSplit.contains("x")
              ? int.parse(strSplit[0]) * int.parse(strSplit[2])
              : strSplit[2] == "0"
                  ? "Operasi tidak bisa dilakukan"
                  : strSplit.contains("/")
                      ? int.parse(strSplit[0]) / int.parse(strSplit[2])
                      // jika ingin hasil bagi nya int maka operasinya menggunakan tanda ~/
                      : "Input anda salah";
}

void main() {
  print(kalkulatorSederhana("2 / 4"));
}
